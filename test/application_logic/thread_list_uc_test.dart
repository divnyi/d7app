import 'package:flutter_test/flutter_test.dart';
import 'package:d7app/exports.dart';

import 'package:d7app/application_logic/thread_list_uc.dart';

void main() {
  BaseNetwork.apiKey = apiKey;
  test('threads', () async {
    final uc = ThreadListUC.constructor(27);
    final completer = uc.threads.update.nextUpdateCompleter();
    expect(uc.threads.length(), 0);
    await completer.future;
    expect(uc.threads.length() > 0, true);
    expect(uc.threads[0] != null, true);
  });
}
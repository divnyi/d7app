import 'package:flutter_test/flutter_test.dart';
import 'package:d7app/exports.dart';

import 'package:d7app/application_logic/forum_main_uc.dart';

void main() {
  BaseNetwork.apiKey = apiKey;
  test('node tree', () async {
    final uc = ForumMainUC.constructor();
    final completer = uc.nodeTree.nextUpdateCompleter();
    expect(uc.nodeTree.value.value, null);
    expect(uc.nodeTree.value.children.length, 0);
    await completer.future;
    expect(uc.nodeTree.value.value, null);
    expect(uc.nodeTree.value.children.length >= 5, true);
  });
}
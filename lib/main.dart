import 'package:flutter/material.dart';
import 'exports.dart';

import 'package:d7app/ui/forum_main_widget.dart';
import 'package:d7app/application_logic/global.dart';

void main() {
  BaseNetwork.apiKey = apiKey;
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final global = Global.constructor();
    final materialApp = MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
        backgroundColor: global.colors.background,
        scaffoldBackgroundColor: global.colors.background,
        primaryTextTheme: TextTheme(
          title: TextStyle(
            color: global.colors.appBarText,
          ),
        ),
        accentTextTheme: TextTheme(
          subhead: TextStyle(
            color: global.colors.categoryText,
          ),
        ),
        appBarTheme: AppBarTheme(
            color: global.colors.appBarBg
        ),
      ),
      home: ForumMainWidget(),
    );
    final globalSM = global.widget(materialApp);
    return globalSM;
  }
}

class Tree<Type> {
  Type value;
  List<Tree<Type>> children;

  Tree(this.value, this.children);
}
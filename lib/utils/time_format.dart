import 'package:intl/intl.dart';

String timeString(DateTime d) {
  var formatter = new DateFormat.Hm();
  String formatted = formatter.format(d);
  return formatted;
}

String shortDateString(DateTime d) {
  String monthStr;
  switch (d.month) {
    case 1: monthStr = "січня"; break;
    case 2: monthStr = "лютого"; break;
    case 3: monthStr = "березня"; break;
    case 4: monthStr = "квітня"; break;
    case 5: monthStr = "травня"; break;
    case 6: monthStr = "червня"; break;
    case 7: monthStr = "липня"; break;
    case 8: monthStr = "серпня"; break;
    case 9: monthStr = "вересня"; break;
    case 10: monthStr = "жовтня"; break;
    case 11: monthStr = "листопада"; break;
    case 12: monthStr = "грудня"; break;
  }
  return "${d.day} $monthStr";
}

String dateString(DateTime d) {
  var formatter = new DateFormat("dd.MM.yyyy");
  String formatted = formatter.format(d);
  return formatted;
}

String flexibleTimeString(DateTime d) {
  final now = DateTime.now();
  final thisDay = DateTime(now.year, now.month, now.day);
  final thisYear = DateTime(now.year);
  if (d.isAfter(thisDay)) {
    return timeString(d);
  } else if (d.isAfter(thisYear)) {
    return shortDateString(d);
  } else {
    return dateString(d);
  }
}
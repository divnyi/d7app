import 'package:d7forum_api/d7forum_api.dart';
import 'package:observe/observe.dart';

import 'package:d7app/utils/tree.dart';

abstract class ForumMainUC {
  Observe<Tree<Node>> nodeTree;
  Future<void> fetch();

  static final _defaultConstructor = () => ForumMainUCImpl();
  static var constructor = _defaultConstructor;
  static void setup() { constructor = _defaultConstructor; }
}

class ForumMainUCImpl extends ForumMainUC {
  final nodeTree = Observe(Tree<Node>(null, []));

  ForumMainUCImpl() {
    fetch();
  }

  Future<void> fetch() async {
    final nodes = await API.shared.nodes.fetch();
    this.nodeTree.value = formTree(null, nodes);
  }

  Tree<Node> formTree(Node parentNode, List<Node> nodes) {
    final parentNodeId = parentNode?.nodeId ?? 0;
    final children = nodes
        .where((arg) => arg.parentNodeId == parentNodeId)
        .map((arg) => formTree(arg, nodes))
        .toList();
    return Tree<Node>(parentNode, children);
  }
}
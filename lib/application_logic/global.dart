import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
export 'package:scoped_model/scoped_model.dart';

import 'package:d7app/constants/colors.dart';

abstract class Global extends Model {
  AppColors colors;

  static final _defaultConstructor = () => GlobalImpl();
  static var constructor = _defaultConstructor;
  static void setup() { constructor = _defaultConstructor; }
 
  static Global of(BuildContext context) =>
      ScopedModel.of<Global>(context);
  Widget widget(Widget child) =>
      ScopedModel<Global>(model: this, child: child);
}

class GlobalImpl extends Global {
  AppColors colors = AppColors.light();
}
import 'package:d7forum_api/d7forum_api.dart';
import 'package:observe/observe.dart';

import 'package:d7app/repository/thread_repo.dart';

abstract class ThreadListUC {
  LazyArray<Thread> threads;

  Future<void> fetch();

  static final _defaultConstructor = (int forumId) => ThreadListUCImpl(forumId);
  static var constructor = _defaultConstructor;
  static void setup() { constructor = _defaultConstructor; }
}

class ThreadListUCImpl extends ThreadListUC {
  ThreadRepo repo = ThreadRepo.constructor();
  int forumId;

  Pagination _lastPagination;

  ThreadListUCImpl(this.forumId) {
    final threadLazy = repo.threads.asyncAction((idx, thread) {
      if (thread != null) return null;
      if (_lastPagination == null) return null;
      return Future(() async {
        await this.fetchAtIdx(idx);
      });
    });
    final withSticky = threadLazy.transform<int, Thread>(
        newLengthFn: () => repo.sticky.length() + repo.threads.length(),
        key: (newIdx) {
          final idx = newIdx - repo.sticky.length();
          if (idx < 0) return null;
          return idx;
        },
        newValue: (newIdx) => repo.sticky[newIdx]
    );
    repo.sticky.update.addListener(() {
      withSticky.update.performUpdate();
    });
    this.threads = withSticky;

    fetch();
  }

  Future<void> fetch() async {
    final forumThreads = await API.shared.forumThreads.fetch(forumId);
    this._lastPagination = forumThreads.pagination;
    repo.update(forumThreads);
  }

  Future<void> fetchAtIdx(int idx) async {
    final page = 1 + (idx / _lastPagination.perPage).floor();
    final forumThreads = await API.shared.forumThreads.fetch(forumId, page: page);
    this._lastPagination = forumThreads.pagination;
    repo.update(forumThreads);
  }
}
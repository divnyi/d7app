import 'package:flutter/material.dart';
import 'package:d7app/exports.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';

import 'package:d7app/application_logic/forum_main_uc.dart';
import 'package:d7app/utils/tree.dart';
import 'package:d7app/application_logic/global.dart';
import 'reusable/simple_reusable.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'thread_list_widget.dart';

class ForumMainWidget extends StatelessWidget {
  final useCase = ForumMainUC.constructor();

  @override
  Widget build(BuildContext context) =>
      Scaffold(
        body: refreshIndicator(context),
      );

  Widget refreshIndicator(BuildContext context) =>
      RefreshIndicator(
        onRefresh: useCase.fetch,
        child: tree(context),
      );

  Widget tree(BuildContext context) =>
      useCase.nodeTree.toWidget((context, tree) =>
          firstLayerList(context, tree));

  Widget firstLayerList(BuildContext context, Tree<Node> tree) =>
      CustomScrollView(
        slivers: List<Widget>()
            + [appBarSliver(context)]
            + firstLayerNodes(context, tree),
      );

  Widget appBarSliver(BuildContext context) =>
      SimpleReusable.bigAppBarSliver("Форум\nДемократична Сокира");

  /// categories
  List<Widget> firstLayerNodes(BuildContext context, Tree<Node> tree) {
    final nodes = tree.children.map((tree) => tree)
        .where((tree) => tree.value is Category);
    final widgets = nodes.map((tree) =>
        SliverStickyHeader(
          header: categoryHeader(context, tree.value),
          sliver: new SliverList(
            delegate: SliverChildListDelegate([
              secondLayerList(context, tree),
            ]),
          ),
        )
    ).toList();
    return widgets;
  }

  Widget categoryHeader(BuildContext context, Category category) =>
      Container(
        height: 30.0,
        child: DecoratedBox(
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: [
                Global.of(context).colors.categoryBg1,
                Global.of(context).colors.categoryBg2,
              ], begin: Alignment.topLeft, end: Alignment(0.5, 1.0)),
              color: Colors.red
          ),
          child: Align(
            child: Text(category.title,
              style: Theme.of(context).accentTextTheme.subhead,
            ),
            alignment: Alignment(-0.9, 0.0),
          ),
        ),
      );

  Widget secondLayerList(BuildContext context, Tree<Node> tree) =>
      Column(
        children: secondLayerNodes(context, tree),
      );

  /// forums
  List<Widget> secondLayerNodes(BuildContext context, Tree<Node> tree) {
    final nodes = tree.children.map((tree) => tree)
        .where((tree) => tree.value is Forum);
    final widgets = nodes.map((tree) {
      final forum = tree.value as Forum;
      final widget = SimpleReusable.forumListTile(context, forum, () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ThreadListWidget(tree)),
        );
      });
      final combinedThreadWidget = Column(
        children: <Widget>[widget] + thirdLayerNodes(context, tree),
      );
      return combinedThreadWidget;
    }
    ).toList();
    return widgets;
  }

  /// sub-forums
  List<Widget> thirdLayerNodes(BuildContext context, Tree<Node> tree) {
    final nodes = tree.children.map((tree) => tree.value);
    final widgets = nodes.map((tree) => Row(
      children: <Widget>[
        Spacer(flex: 9,),
        Icon(FontAwesomeIcons.comments, size: 14),
        Spacer(flex: 1,),
        Text(tree.title),
        Spacer(flex: 30,),
      ],
    )).toList();
    return widgets;
  }
}
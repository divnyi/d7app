import 'package:d7forum_api/d7forum_api.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:d7app/utils/time_format.dart' as timeFormat;
import 'package:d7app/application_logic/global.dart';
import 'package:auto_size_text/auto_size_text.dart';

import 'package:flutter/material.dart';

class SimpleReusable {
  static TextStyle titleTextStyle() => TextStyle(shadows: [Shadow(offset: Offset(2.0, 2.0), blurRadius: 3.0)]);

  static Widget bigAppBarSliver(String title) => SliverAppBar(
    expandedHeight: 130.0,
    flexibleSpace: FlexibleSpaceBar(
      centerTitle: true,
      background: Image.asset('assets/face_illustr.jpg', fit: BoxFit.cover),
      title: Text(title,
        textAlign: TextAlign.center,
        style: titleTextStyle(),
      ),
    ),
    pinned: true,
  );

  static Widget smallAppBarSliver(String title) => SliverAppBar(
    expandedHeight: 50.0,
    flexibleSpace: FlexibleSpaceBar(
//      centerTitle: true,
      title: Align(
        alignment: Alignment(-0.15, 1.0),
          child: AutoSizeText(title,
            textAlign: TextAlign.center,
            style: titleTextStyle(),
          )
      ),
      titlePadding: EdgeInsets.fromLTRB(50, 20, 20, 16),
    ),
    pinned: true,
  );

  static Widget forumListTile(BuildContext context, Forum forum, void onTap()) =>
      ListTile(
        onTap: onTap,
        title: Text(forum.title),
        subtitle:
        RichText(
          maxLines: 3,
          overflow: TextOverflow.ellipsis,
          text: TextSpan(
            style: Theme.of(context).textTheme.caption,
            children: <TextSpan>[
              TextSpan(text: forum.lastPostUsername, style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Global.of(context).colors.nameColor,
              )),
              TextSpan(text: " в ", style: TextStyle(
                color: Theme.of(context).textTheme.body1.color,
              )),
              TextSpan(text: forum.lastThreadTitle),
            ],
          ),
        ),
        trailing: Column(children: <Widget>[
          Spacer(flex: 5,),
          Text(timeFormat.flexibleTimeString(forum.lastPostDate),
            style: Theme.of(context).textTheme.caption,
          ),
          Spacer(flex: 5),
          Text("${forum.discussionCount} тем",
            style: Theme.of(context).textTheme.overline,
          ),
          Spacer(flex: 1,),
          Text("${forum.messageCount} реплік",
            style: Theme.of(context).textTheme.overline,
          ),
          Spacer(flex: 2,),
        ],
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.end,
        ),
        leading: Icon(FontAwesomeIcons.comments, size: 40,),
      );
}
import 'package:flutter/material.dart';
import 'package:d7app/exports.dart';

import 'package:d7app/application_logic/thread_list_uc.dart';
import 'package:d7app/application_logic/global.dart';
import 'reusable/simple_reusable.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:d7app/utils/tree.dart';

class ThreadListWidget extends StatelessWidget {
  Tree<Node> node;
  Forum get forum => node.value as Forum;
  ThreadListUC useCase;

  ThreadListWidget(this.node) {
    this.useCase = ThreadListUC.constructor(this.forum.nodeId);
  }

  @override
  Widget build(BuildContext context) =>
      Scaffold(
        body: refreshIndicator(context),
      );

  Widget refreshIndicator(BuildContext context) =>
      RefreshIndicator(
        onRefresh: useCase.fetch,
        child: scrollView(context),
      );

  Widget scrollView(BuildContext context) =>
      CustomScrollView(slivers: <Widget>[
        SimpleReusable.smallAppBarSliver(forum.title),
        subforumsSliver(context),
        threadsSliver(context),
      ],);

  Widget threadsSliver(BuildContext context) =>
      useCase.threads.toUpdatesListener((context) =>
          SliverList(delegate: SliverChildBuilderDelegate(
            useCase.threads
                .toWidgetBuilder((context, idx, thread) =>
                threadWidget(context, thread)),
            childCount: useCase.threads.length(),
          ))
      );

  Widget threadWidget(BuildContext context, Thread thread) =>
  thread == null ?
  ListTile() :
  ListTile(
    title: Text(thread.title),
  );

  Widget subforumsSliver(BuildContext context) =>
      SliverList(delegate: SliverChildListDelegate(subforums(context)));

  List<Widget> subforums(BuildContext context) {
    final nodes = this.node.children.map((tree) => tree)
        .where((tree) => tree.value is Forum);
    final widgets = nodes.map((tree) {
      final forum = tree.value as Forum;
      final widget = SimpleReusable.forumListTile(context, forum, () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ThreadListWidget(tree)),
        );
      });
      return widget;
    }).toList();
    return widgets;
  }
}
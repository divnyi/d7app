import 'package:flutter/material.dart';

class ColorPalette {
  static const d7red = Color.fromRGBO(128, 23, 33, 1.0);
  static const darkerRed = Color.fromRGBO(70, 10, 15, 1.0);
  static const d7yellowText = Color.fromRGBO(232, 220, 200, 1.0);
  static const d7lightYellowBg = Color.fromRGBO(255, 248, 236, 1.0);
  static const d7blueText = Color.fromRGBO(73, 146, 183, 1.0);
}

class AppColors {
  final Color background;
  final Color appBarBg;
  final Color appBarText;
  final Color categoryBg1;
  final Color categoryBg2;
  final Color categoryText;
  final Color nameColor;
  AppColors.light({
    this.background = ColorPalette.d7lightYellowBg,
    this.appBarBg = ColorPalette.d7red,
    this.appBarText = ColorPalette.d7lightYellowBg,
    this.categoryText = ColorPalette.d7yellowText,
    this.categoryBg1 = ColorPalette.d7red,
    this.categoryBg2 = ColorPalette.darkerRed,
    this.nameColor = ColorPalette.d7blueText,
  });
}
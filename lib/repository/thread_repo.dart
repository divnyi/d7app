import 'package:d7forum_api/d7forum_api.dart';
import 'package:observe/observe.dart';

abstract class ThreadRepo {
  LazyArray<Thread> sticky;
  LazyArray<Thread> threads;
  void update(ForumThreads threads);

  static final _defaultConstructor = () => ThreadRepoImpl();
  static var constructor = _defaultConstructor;
  static void setup() { constructor = _defaultConstructor; }
}

class ThreadRepoImpl extends ThreadRepo {
  List<Thread> stickyDB = <Thread>[];
  List<Thread> threadDB = <Thread>[];
  int maxItems;

  ThreadRepoImpl() {
    this.threads = LazyArray<Thread>(
      length: () => maxItems ?? threadDB.length,
      item: (idx) {
        return threadDB[idx];
      }
    ).safeguardType<RangeError>(() => null);
    this.sticky = LazyArray<Thread>(
        length: () => stickyDB.length,
        item: (idx) {
          return stickyDB[idx];
        }
    );
  }

  void update(ForumThreads threads) {
    this.maxItems = threads.pagination.total;

    if (threads.sticky.length > 0) {
      final sticky = Set<Thread>()
        ..addAll(this.stickyDB)
        ..addAll(threads.sticky);
      final stickyArray = sticky.toList()
        ..sort((t1, t2) =>
        t2.lastPostDate.millisecondsSinceEpoch
            - t1.lastPostDate.millisecondsSinceEpoch);
      this.stickyDB = stickyArray;
      this.sticky.update.performUpdate();
    }

    if (threads.threads.length > 0) {
      final thread = Set<Thread>()
        ..addAll(this.threadDB)
        ..addAll(threads.threads);
      final threadArray = thread.toList()
        ..sort((t1, t2) =>
        t2.lastPostDate.millisecondsSinceEpoch
            - t1.lastPostDate.millisecondsSinceEpoch);
      this.threadDB = threadArray;
      this.threads.update.performUpdate();
    }
  }
}